<?php

namespace Npontu\USpy;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ClickStreamMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $response = $next($request);
        $app = config('app.name');
        $url = $request->fullUrl();
        $method = $request->method();
        $user_agent = $request->header('User-Agent');
        $ip_address = $request->ip();
        $payload = $request->all();
        $fingerprint = $request->fingerprint();
        $user_id = auth()->id() ?? null;
        $user_obj = json_encode(auth()->user());
        $responseContent = $response->getContent();

        $requestData = [
            'app' => $app,
            'url' => $url,
            'method' => $method,
            'user_agent' => $user_agent,
            'ip_address' => $ip_address,
            'payload' => $payload,
            'fingerprint' => $fingerprint,
            'user_id' => $user_id,
            'user_obj' => $user_obj,
            'response' => $responseContent
//            'response' => $this->isJson($responseContent) ? $responseContent : null
        ];

        $this->logService($requestData);

        return $response;
    }

    public function logService($response, $type = 'REQUEST')
    {
        $data = json_encode($response);
        $now = now();
        $path = storage_path('logs');
        if (!file_exists($path)) //if directory does not exist
            shell_exec("mkdir $path");
        file_put_contents(storage_path('logs/uspy.log'), "-------\n" . '' . $type . ': ' . $now . ' || ' . $data . PHP_EOL, FILE_APPEND);
    }

    function isJson($string)
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}
